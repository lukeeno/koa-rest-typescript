import * as jwt from "jsonwebtoken"
import { ParameterizedContext } from "koa"
export default (ctx: ParameterizedContext) => {
    if (ctx.request.body.password === "password") {
        ctx.status = 200
        ctx.body = {
            message: "Successful Authentication",
            token: jwt.sign(
                {
                    role: "admin",
                },
                "YourKey",
            ), // Store this key in an environment variable
        }
    } else {
        ctx.status = 401
        ctx.body = {
            message: "Authentication Failed",
        }
    }
    return ctx
}
