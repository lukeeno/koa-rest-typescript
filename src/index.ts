import * as Koa from "koa"
import * as bodyParser from "koa-bodyparser"
import * as helmet from "koa-helmet"
import * as logger from "koa-logger"
import * as mongoose from "mongoose"
import { connexionString, port } from "./config"
import routing from "./routes"

mongoose.connect(connexionString, { useNewUrlParser: true, useFindAndModify: false })
// tslint:disable-next-line: no-console
mongoose.connection.on("error", console.error)

const app = new Koa()

app
    .use(logger())
    .use(bodyParser())
    .use(helmet())

routing(app)

if (!module.parent) {
    app.listen(port, () =>
        // tslint:disable-next-line: no-console
        console.log(`✔ The server is running at http://localhost:${port}/`),
    )
}

export default app
