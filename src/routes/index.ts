import * as Koa from "koa"
import * as Router from "koa-router"
import routesLoader from "../utils/routesLoader"

export default function(app: Koa) {
    routesLoader(`${__dirname}`).then((routers: Router[]) => {
        routers.forEach((route) => {
            app.use(route.routes()).use(
                route.allowedMethods({
                    throw: true,
                }),
            )
        })
        // tslint:disable-next-line: no-console
    }).catch(console.error)
}
