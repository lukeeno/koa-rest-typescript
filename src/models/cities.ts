import { model, Schema } from "mongoose"

// To fix https://github.com/Automattic/mongoose/issues/4291
// tslint:disable-next-line: no-var-requires
require("mongoose").Promise = global.Promise

const citySchema = new Schema({
    country: String,
    name: {
        required: true,
        type: String,
    },
    totalPopulation: {
        required: true,
        type: Number,
    },
    updated: {
        default: Date.now,
        type: Date,
    },
    zipCode: Number,
})

export default model("City", citySchema)
