import { expect, should } from "chai"
import * as mongoose from "mongoose"
import * as supertest from "supertest"
import app from ".."

const temp: { idCity: string, token: string } = {
    idCity: null,
    token: null,
}
const server = app.listen()
const request = supertest.agent(server)
should()

after(() => {
    mongoose.connection.close()
    server.close()
})

describe("POST api/authenticate", () => {
    it("should recieved auth token", (done) => {
        request
            .post("/api/authenticate")
            .set("Accept", "application/json")
            .send({
                password: "password",
            })
            .expect(200, (_, res) => {
                temp.token = res.body.token
                done()
            })
    })
})

describe("POST /city", () => {
    it("should add a city", (done) => {
        request
            .post("/api/cities")
            .set("Accept", "application/json")
            .set("Authorization", `Bearer ${temp.token}`)
            .set("Accept", "application/json")
            .send({
                country: "Thailand",
                name: "Bangkok",
                totalPopulation: 8249117,
                zipCode: 1200,
            })
            .expect(200, (_, res) => {
                temp.idCity = res.body._id
                done()
            })
    })
})

describe("GET /cities", () => {
    it("should get all cities", (done) => {
        request
            .get("/api/cities")
            .set("Authorization", `Bearer ${temp.token}`)
            .set("Accept", "application/json")
            .expect(200, (_, res) => {
                expect(res.body.length).to.be.at.least(1)
                done()
            })
    })
})

describe("GET /cities/:id", () => {
    it("should get a city", (done) => {
        request
            .get(`/api/cities/${temp.idCity}`)
            .set("Authorization", `Bearer ${temp.token}`)
            .set("Accept", "application/json")
            .expect(200, (_, res) => {
                res.body.name.should.equal("Bangkok")
                res.body.totalPopulation.should.equal(8249117)
                res.body.country.should.equal("Thailand")
                res.body.zipCode.should.equal(1200)
                res.body._id.should.equal(temp.idCity)
                done()
            })
    })
})

describe("PUT /cities", () => {
    it("should update a city", (done) => {
        request
            .put(`/api/cities/${temp.idCity}`)
            .set("Authorization", `Bearer ${temp.token}`)
            .set("Accept", "application/json")
            .send({
                country: "Thailand",
                name: "Chiang Mai",
                totalPopulation: 148477,
                zipCode: 50000,
            })
            .expect(200, (_, res) => {
                temp.idCity = res.body._id
                done()
            })
    })

    it("should get updated city", (done) => {
        request
            .get(`/api/cities/${temp.idCity}`)
            .set("Authorization", `Bearer ${temp.token}`)
            .set("Accept", "application/json")
            .expect(200, (_, res) => {
                res.body.name.should.equal("Chiang Mai")
                res.body.totalPopulation.should.equal(148477)
                res.body.country.should.equal("Thailand")
                res.body.zipCode.should.equal(50000)
                res.body._id.should.equal(temp.idCity)
                done()
            })
    })
})

describe("DELETE /cities", () => {
    it("should delete a city", (done) => {
        request
            .delete(`/api/cities/${temp.idCity}`)
            .set("Authorization", `Bearer ${temp.token}`)
            .set("Accept", "application/json")
            .expect(200, (_, res) => {
                done()
            })
    })

    it("should get error", (done) => {
        request
            .get(`/api/cities/${temp.idCity}`)
            .set("Accept", "application/json")
            .expect(404, () => {
                done()
            })
    })
})
