import * as glob from "glob"
import * as Router from "koa-router"

export default function(dirname: string) {
    return new Promise((resolve, reject) => {
        const routes: Router[] = []
        glob(
            `${dirname}/**/*.ts`,
            {
                ignore: "**/index.ts",
            },
            (err: Error, files: string[]) => {
                if (err) {
                    return reject(err)
                }

                files.forEach((file) => {
                    const route = require(file)

                    if (route.default === undefined || !(route.default instanceof Router)) {
                        reject(new Error("Route must return instance of Router from koa-router package."))
                    }

                    routes.push(route.default)
                })

                return resolve(routes)
            },
        )
    })
}
