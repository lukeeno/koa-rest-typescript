# Koa - REST - TypeScript

Simple Koa project boilerplate for REST API.

## Technologies

- Koa v2, Koa router
- TypeScript, TSLint
- MongoDB, Mongoose
- Mocha, Chai, supertest

## Influenced by

- [Node - Koa - Typescript Project](https://github.com/javieraviles/node-typescript-koa-rest)
- [Koa REST API Boilerplate](https://github.com/posquit0/koa-rest-api-boilerplate)
